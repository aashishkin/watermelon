﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace watermelon
{
    class Program
    {
        static void Main(string[] args)
        {
            int max = 0, min = 0;
            Console.WriteLine("How many watermelons?");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine();
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"Вес арбуза №{i+1}");
                int temp = int.Parse(Console.ReadLine());
                if (i==0)
                {
                    max = temp;
                    min = temp;
                }
                else if (temp > max)
                {
                    max = temp;
                }
                else if (temp < min)
                {
                    min = temp;
                }
            }
            Console.WriteLine(min + " " + max);
            Console.ReadKey();
        }
    }
}
